import argparse
import time
from pathlib import Path

import cv2
from loguru import logger
import tensorflow as tf

from src.command import common_args, parse_args
from src.worldmodels.controller import VAEController
from src.worldmodels.data_loader import DataLoader
from src.worldmodels.model import ConvVAE


def main(args: dict):
    log_dir = args["tensorboard_dir"] / time.strftime("VAE_%H%M%S")
    log_dir.mkdir(parents=True, exist_ok=True)

    dataset = DataLoader(
        image_dir=args["training_data_dir"], batch_size=args["batch_size"],
    )

    vae = ConvVAE(
        z_size=args["z_size"],
        batch_size=args["batch_size"],
        learning_rate=0.0003,
        kl_tolerance=0.5,
        beta=1.0,  # weight for KL loss
        is_training=True,
        reuse=False,
    )
    vae_controller = VAEController(z_size=args["z_size"])
    vae_controller.vae = vae
    merged = tf.compat.v1.summary.merge(
        [
            tf.compat.v1.summary.scalar("vae/loss", vae.loss),
            tf.compat.v1.summary.scalar("vae/r_loss", vae.r_loss),
            tf.compat.v1.summary.scalar("vae/kl_loss", vae.kl_loss),
        ]
    )
    assert merged is not None
    writer = tf.compat.v1.summary.FileWriter(log_dir, graph=vae.graph, flush_secs=10,)
    try:
        logger.info(f"Learning. CTRL+C to quit.")
        for step, obs in enumerate(dataset):
            feed = {vae.input_tensor: obs}
            (summary, train_step, _) = vae.sess.run(
                [merged, vae.global_step, vae.train_op,], feed,
            )
            writer.add_summary(summary, step)

            if step % 100 == 0:
                print(f"VAE optimisation step: {step}")
                vae_controller.set_target_params()
                image = (
                    obs[0] * 255
                )  # We've normalised these images. In reality they won't be.
                encoded = vae_controller.encode(image)
                reconstructed_image = vae_controller.decode(encoded)[0]
                cv2.imwrite(str(log_dir / f"Original_{step}.jpg"), image)
                cv2.imwrite(
                    str(log_dir / f"Reconstruction_{step}.jpg"), reconstructed_image,
                )

    finally:
        logger.info(f'Saving vae to {args["vae_path"]}, don\'t quit!')
        vae_controller.set_target_params()
        vae_controller.save(args["vae_path"])
        writer.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Train a VAE based upon a directory of DonkeyCar images."
    )
    parser.add_argument(
        "--z_size",
        type=int,
        default=32,
        help="Size of the latent dimension in the VAE.",
    )
    parser.add_argument(
        "--batch_size", type=int, default=32, help="Batch size when training VAE",
    )
    parser.add_argument(
        "--training_data_dir",
        type=Path,
        help="Directory containing training data images from donkey_sim (dir).",
    )
    parser = common_args(parser)
    main(parse_args(parser))
